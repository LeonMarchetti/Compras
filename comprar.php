<?php
header('Content-Type: application/json');

include "conexion_bd.php";

$respuesta = new \stdClass();

$carrito = $_POST["carrito"];

$conexion = getConexion();
if ($conexion === NULL) {
    // No se pudo conectar a la base de datos.
    $respuesta->error = $conexion->connect_error;
} else {
    $conexion->autocommit(false);

    $id_usuario = $_POST["usuario"];

    // Insertar compra:
    if ($sentencia_compra = $conexion->prepare("Insert Into Compra (id_usuario) Values (?)")) {
        $sentencia_compra->bind_param("i", $id_usuario);

        if ($sentencia_compra->execute() && $sentencia_compra->affected_rows) {
            $id_compra = $conexion->insert_id;
            $sentencia_compra->free_result();

            // Insertar productos en los detalles:
            if ($sentencia_detalle = $conexion->prepare("Insert Into Detalle (id_compra, id_producto, precio, cantidad) Values (?, ?, ?, ?)")) {
                $sentencia_detalle->bind_param("iidi", $id_compra, $id_producto, $precio, $cantidad);

                $error_sesion = false;

                $productos_comprados = 0;
                foreach ($carrito as $producto) {
                    $id_producto = $producto["id"];
                    $precio      = $producto["precio"];
                    $cantidad    = $producto["cantidad"];

                    if (!$sentencia_detalle->execute()) {
                        // Si hubo un error al insertar un detalle entonces
                        // termino y hago un rollback:
                        error_log("[".__LINE__."] $conexion->error");
                        $respuesta->error = "Error en la compra";
                        $error_sesion = true;
                        break;
                    }

                    $productos_comprados++;
                }

                // Comprobar errores:
                if ($error_sesion) {
                    $conexion->rollback();
                } else {
                    $conexion->commit();
                    $respuesta->mensaje = "Productos comprados: $productos_comprados";
                }

                $sentencia_detalle->free_result();
            } else {
                $sentencia_compra->free_result();
                error_log("[".__LINE__."] $conexion->error");
                $respuesta->error = "Error en la compra";
            } // if ($sentencia_detalle = $conexion->prepare(...))

        } else {
            $sentencia_compra->free_result();
            error_log("[".__LINE__."] $conexion->error");
            $respuesta->error = "Error en la compra";
        } // if ($sentencia_compra->execute(...))

    } else {
        error_log("[".__LINE__."] $conexion->error");
        $respuesta->error = "Error en la compra";
    } // if ($sentencia_compra = $conexion->prepare(...))
}
$conexion->close();
echo json_encode($respuesta);
?>