const MANEJO_SESION = "manejo_sesion.php";

Vue.component("manejo-sesion", {
    template: `
    <div v-if="id">
        <!-- Usuario con sesión iniciada -->
        Sesión iniciada como <b>{{ usuario }}</b>.<br>
        <button @click="cerrar_sesion">Cerrar Sesión</button><br>
    </div>
    
    <div v-else>
        <!-- Usuario sin sesión iniciada -->
        Inicie Sesión<br>
        <input id="input_nombre" placeholder="Nombre de usuario"><br>
        <input id="input_contrasena" type="password" placeholder="Contraseña"><br>
        <button @click="iniciar_sesion">Iniciar Sesión</button><br>
        <span id="span_error" v-if="error"><b>Error</b> {{ error }}.</span>
    </div>`,
    data: function() {
        return {
            error:   "",
            id:      null,
            usuario: null,
        }
    },
    mounted: function() {
        $.post(MANEJO_SESION, { "action": "check" }, (resp) => {
            this.error = resp["error"];

            if (resp["id"]) {
                this.id      = resp["id"];
                this.usuario = resp["usuario"];
                this.$emit("id", this.id);
            }
        });
    },
    methods: {
        cerrar_sesion: function() {
            $.post(MANEJO_SESION, { "action": "logout" }, () => {
                this.id = null;
                this.$emit("id", this.id);
            });
        },
        iniciar_sesion: function() {
            this.error = "";

            var nombre_usuario = $("#input_nombre").val();

            var credenciales = {
                "action": "login",
                "user":   nombre_usuario,
                "pass":   $("#input_contrasena").val(),
            };
            
            $.post(MANEJO_SESION, credenciales, (resp) => {
                if (resp["error"]) {
                    this.error   = resp["error"];
                } else {
                    this.id      = resp["id"];
                    this.usuario = resp["usuario"];

                    this.$emit("id", this.id);
                }
            });
        },
    }
});