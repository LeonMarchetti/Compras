<?php

function getConexion() {
    // Leo el archivo con las credenciales:
    $archivo = json_decode(file_get_contents("config/.db.json"));
    
    $host = $archivo->host;
    $user = $archivo->user;
    $pass = $archivo->pass;
    $base = $archivo->base;
    $port = $archivo->port;

    $conexion = new mysqli($host, $user, $pass, $base, $port);
    if ($conexion->connect_error) {
        return NULL;
    }
    return $conexion;
}

?>