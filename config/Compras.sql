-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-06-2019 a las 21:57:11
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Compras`
--
CREATE DATABASE IF NOT EXISTS `Compras` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `Compras`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AutenticacionUsuario`
--
-- Creación: 23-06-2019 a las 18:08:19
-- Última actualización: 23-06-2019 a las 18:37:36
--

DROP TABLE IF EXISTS `AutenticacionUsuario`;
CREATE TABLE `AutenticacionUsuario` (
  `id_usuario` int(11) NOT NULL,
  `sal` varchar(16) NOT NULL,
  `hash` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CategoriaProducto`
--
-- Creación: 23-06-2019 a las 21:17:20
-- Última actualización: 23-06-2019 a las 21:18:33
--

DROP TABLE IF EXISTS `CategoriaProducto`;
CREATE TABLE `CategoriaProducto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Compra`
--
-- Creación: 23-06-2019 a las 20:36:18
-- Última actualización: 23-06-2019 a las 20:47:40
--

DROP TABLE IF EXISTS `Compra`;
CREATE TABLE `Compra` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `estado` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle`
--
-- Creación: 23-06-2019 a las 20:43:41
-- Última actualización: 23-06-2019 a las 20:47:40
--

DROP TABLE IF EXISTS `Detalle`;
CREATE TABLE `Detalle` (
  `id_compra` int(11) NOT NULL,
  `nro` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `Detalle`
--
DROP TRIGGER IF EXISTS `TRG_AD_DETALLE`;
DELIMITER $$
CREATE TRIGGER `TRG_AD_DETALLE` AFTER DELETE ON `Detalle` FOR EACH ROW Update Compra
	Set total = total - Old.precio * Old.cantidad
    Where id = Old.id_compra
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_AI_DETALLE`;
DELIMITER $$
CREATE TRIGGER `TRG_AI_DETALLE` AFTER INSERT ON `Detalle` FOR EACH ROW Update Compra
    Set total = total + New.precio * New.cantidad
    Where id = New.id_compra
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_AU_DETALLE`;
DELIMITER $$
CREATE TRIGGER `TRG_AU_DETALLE` AFTER UPDATE ON `Detalle` FOR EACH ROW Begin

Update Compra
    Set total = total - Old.precio * Old.cantidad
    Where Compra.id = Old.id_compra;

Update Compra
    Set total = total + New.precio * New.cantidad
    Where Compra.id = New.id_compra;
    
End
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_BI_DETALLE`;
DELIMITER $$
CREATE TRIGGER `TRG_BI_DETALLE` BEFORE INSERT ON `Detalle` FOR EACH ROW Set New.precio := (
    Select precio 
    From Producto 
    Where id = New.id_producto
)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Domicilio`
--
-- Creación: 23-06-2019 a las 18:08:21
--

DROP TABLE IF EXISTS `Domicilio`;
CREATE TABLE `Domicilio` (
  `dni` int(11) NOT NULL,
  `nro` int(11) NOT NULL,
  `calle` varchar(20) NOT NULL,
  `numero` int(11) NOT NULL,
  `piso` varchar(30) NOT NULL,
  `cod_postal` varchar(30) NOT NULL,
  `provincia` int(11) NOT NULL,
  `localidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Persona`
--
-- Creación: 23-06-2019 a las 18:08:15
-- Última actualización: 23-06-2019 a las 18:32:52
--

DROP TABLE IF EXISTS `Persona`;
CREATE TABLE `Persona` (
  `dni` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `fecha_nac` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Producto`
--
-- Creación: 23-06-2019 a las 21:43:27
--

DROP TABLE IF EXISTS `Producto`;
CREATE TABLE `Producto` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(256) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `categoria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Telefono`
--
-- Creación: 23-06-2019 a las 18:08:22
--

DROP TABLE IF EXISTS `Telefono`;
CREATE TABLE `Telefono` (
  `dni` int(11) NOT NULL,
  `cod_pais` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `numero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--
-- Creación: 23-06-2019 a las 18:08:24
-- Última actualización: 23-06-2019 a las 19:27:14
--

DROP TABLE IF EXISTS `Usuario`;
CREATE TABLE `Usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `dni` int(11) DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `ult_login` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `AutenticacionUsuario`
--
ALTER TABLE `AutenticacionUsuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `CategoriaProducto`
--
ALTER TABLE `CategoriaProducto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Compra`
--
ALTER TABLE `Compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `Detalle`
--
ALTER TABLE `Detalle`
  ADD PRIMARY KEY (`id_compra`,`nro`),
  ADD KEY `id_prod` (`id_producto`),
  ADD KEY `nro` (`nro`);

--
-- Indices de la tabla `Domicilio`
--
ALTER TABLE `Domicilio`
  ADD PRIMARY KEY (`dni`,`nro`);

--
-- Indices de la tabla `Persona`
--
ALTER TABLE `Persona`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `Producto`
--
ALTER TABLE `Producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria` (`categoria`);

--
-- Indices de la tabla `Telefono`
--
ALTER TABLE `Telefono`
  ADD PRIMARY KEY (`dni`,`cod_pais`);

--
-- Indices de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_3` (`nombre`),
  ADD KEY `dni` (`dni`),
  ADD KEY `nombre` (`nombre`),
  ADD KEY `nombre_2` (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `CategoriaProducto`
--
ALTER TABLE `CategoriaProducto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Compra`
--
ALTER TABLE `Compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Detalle`
--
ALTER TABLE `Detalle`
  MODIFY `nro` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `AutenticacionUsuario`
--
ALTER TABLE `AutenticacionUsuario`
  ADD CONSTRAINT `AutenticacionUsuario_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `Usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Compra`
--
ALTER TABLE `Compra`
  ADD CONSTRAINT `Compra_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `Usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Detalle`
--
ALTER TABLE `Detalle`
  ADD CONSTRAINT `Detalle_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `Producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Detalle_ibfk_3` FOREIGN KEY (`id_compra`) REFERENCES `Compra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Domicilio`
--
ALTER TABLE `Domicilio`
  ADD CONSTRAINT `Domicilio_ibfk_1` FOREIGN KEY (`dni`) REFERENCES `Persona` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Producto`
--
ALTER TABLE `Producto`
  ADD CONSTRAINT `Producto_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `CategoriaProducto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Telefono`
--
ALTER TABLE `Telefono`
  ADD CONSTRAINT `Telefono_ibfk_1` FOREIGN KEY (`dni`) REFERENCES `Persona` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`dni`) REFERENCES `Persona` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
