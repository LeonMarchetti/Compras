<?php
header('Content-Type: application/json');

include "conexion_bd.php";

$respuesta = new \stdClass();

$conexion = getConexion();
if ($conexion === NULL) {
    // No se pudo conectar a la base de datos.
    $respuesta->error = $conexion->connect_error;
} else {
    if ($resultado = $conexion->query("Select nombre From CategoriaProducto")) {
        $respuesta = [];
        while ($fila = $resultado->fetch_assoc()) {
            $categoria = new \stdClass();
            $categoria->nombre = $fila["nombre"];
            $respuesta[] = $categoria;
        }
    } else {
        // No se pudo realizar la consulta.
        error_log("[".__LINE__."] $conexion->error");
        $respuesta->error = "Error obteniendo categorías de productos.";
    }
}
$conexion->close();
echo json_encode($respuesta);
?>