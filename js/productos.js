const app_productos = new Vue({
    el: "#vue",
    data: {
        carrito:      [],
        categoria_seleccionada: "",
        categorias:   [],
        error:        "",
        error_compra: "",
        filtro:       "",
        id:           null,
        productos:    [], 
    },
    mounted: function() {
        this.obtener_categorias();
        this.obtener_productos();
    },
    methods: {
        agregar_carrito: function(producto) {
            // Me fijo que el producto no esté ya en el carrito:
            for (var i = 0; i < this.carrito.length; i++) {
                if (this.carrito[i].id === producto.id) {
                    return;
                }
            }
            producto.cantidad = 1;
            /* Para ingresar el producto en el carrito tengo que clonarlo, y así
               resuelvo el problema de la reactividad de la tabla. */
            this.carrito.push(JSON.parse(JSON.stringify(producto)));
        },
        comprar: function() {
            this.error_compra = "";

            var datos = {
                "usuario": this.id,
                "carrito": this.carrito,
            };

            $.post("comprar.php", datos, resp => {
                if (resp["error"]) {
                    this.error_compra = resp["error"];
                } else {
                    alert(resp["mensaje"]);
                    console.log(resp["mensaje"]);
                    this.carrito.splice(0, this.carrito.length);
                }
            });
        },
        obtener_categorias: function() {
            this.error = "";
            $.get("categorias.php", (datos) => {
                if (datos["error"]) {
                    this.error = datos["error"];
                } else {
                    this.categorias = datos;
                }
            }).fail((xhr) => {
                this.error = JSON.parse(xhr.responseText)["error"];
            });
        },
        obtener_id_usuario: function(id) {
            this.id = id;
        },
        obtener_productos: function() {
            this.error = "";

            // Armo el URL:
            var url         = "productos.php";
            var querystring = [];

            // Filtro de productos por descripcion:
            if (this.filtro.length > 0) {
                querystring.push(`filtro=${this.filtro}`);
            }

            // Filtro de productos por categoría:
            if (this.categoria_seleccionada) {
                querystring.push(`categoria=${this.categoria_seleccionada}`);
            }

            if (querystring) {
                url += `?${querystring.join("&")}`;
            }

            $.get(url, (datos) => {
                // `datos` es un objeto json
                if (datos["error"]) {
                    // Si hubo un error, el objeto regresado tiene un atributo "error".
                    this.error = datos["error"];
                } else {
                    // Si no hubo errores, el objeto regresado es un arreglo de productos.
                    this.productos = datos;
                }
            }).fail((xhr) => {
                this.error = JSON.parse(xhr.responseText)["error"];
            });
        }, // obtener_productos
        quitar: function(i) {
            this.carrito.splice(i, 1);
        },
        volver: function() {
            window.location = "index.html";
        },
    }, // methods
    watch: {
        categoria_seleccionada: function() {
            this.obtener_productos();
        },
        filtro: function() {
            this.obtener_productos();
        },
    },
    computed: {
        total_compra: function() {
            var total = 0;
            this.carrito.forEach(producto => {
                total += producto.precio * producto.cantidad;
            });
            return total;
        }
    },
    filters: {
        filtro_precio: function(valor) {
            if (!valor && !(typeof valor == "number")) {
                return "";
            }
            return valor.toFixed(2);
        },
    },
});
