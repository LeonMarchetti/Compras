<?php
session_start();

header('Content-Type: application/json');

include "conexion_bd.php";

$respuesta = new \stdClass();

switch ($_POST["action"]) {
    case "check":
        // El navegador comprueba si el usuario tiene una sesión abierta.
        if (isset($_SESSION["userid"])) {
            $respuesta->id = $_SESSION["userid"];
            $respuesta->usuario = $_SESSION["username"];
        } else {
            $respuesta->mensaje = "No hay una sesión iniciada";
        }
        break;

    case "login":
        // El navegador solicita que se inicie sesión.
        $user = $_POST["user"];
        $pass = $_POST["pass"];
    
        $conexion = getConexion();
        if ($conexion === NULL) {
            // No se pudo conectar a la base de datos.
            $respuesta->error = "Error de conexión a la base de datos";
        } else {
            // Preparar consulta
            if ($sentencia = $conexion->prepare("Select id From Usuario u Join AutenticacionUsuario au On u.id = au.id_usuario Where u.nombre = ? And Sha2(au.sal + ?, 512) = au.hash")) {
                $sentencia->bind_param("ss", $user, $pass);
                $sentencia->execute();
                $sentencia->bind_result($id);
                $sentencia->fetch();
                $sentencia->free_result();

                // Si hay resultados entonces se puede iniciar la sesión del usuario
                if ($id) {
                    $_SESSION["userid"] = $id;
                    $_SESSION["username"] = $user;
                    // Actualizar última fecha de inicio de sesión:
                    $resultado = $conexion->query("Update Usuario Set ult_login = Now() Where id = $id");
                    if ($resultado) {
                        $respuesta->id      = $id;
                        $respuesta->usuario = $user;
                    } else {
                        $respuesta->error = $conexion->error;
                    }
                } else {
                    $respuesta->error = "El nombre de usuario o contraseña son inválidos";
                } // if ($id)
            } else {
                // No se pudo realizar la consulta.
                $respuesta->error = $conexion->error;
            }
        } // if ($conexion === NULL)
        break; // case "login"

    case "logout":
        // El navegador solicita que se cierre la sesión.
        unset($_SESSION["userid"]); 
        $respuesta->mensaje = "Se ha cerrado la sesión de $_SESSION[username]";
        break;

    default:
        $respuesta->error = "Acción no reconocida: '".$_POST["action"]."'";
}

echo json_encode($respuesta);
?>