<?php
header('Content-Type: application/json');

include "conexion_bd.php";

$respuesta = new \stdClass();

$conexion = getConexion();
if ($conexion === NULL) {
    // No se pudo conectar a la base de datos.
    $respuesta->error = $conexion->connect_error;

} else {
    $sql = "Select P.id, P.descripcion, P.precio, CP.nombre As categoria";
    $sql .= " From Producto P Join CategoriaProducto CP On P.categoria = CP.id";
    $sql .= " Where P.descripcion Like ? And CP.nombre Like ?";

    if ($sentencia = $conexion->prepare($sql)) {
        // Uso el filtro y la categoría como parámetro en la consulta preparada:
        if (isset($_GET["filtro"])) {
            $filtro = "%$_GET[filtro]%";
        } else {
            $filtro = "%";
        }

        if (isset($_GET["categoria"])) {
            $categoria = "%$_GET[categoria]%";
        } else {
            $categoria = "%";
        }
        
        // Ejecutar consulta preparada:
        $sentencia->bind_param("ss", $filtro, $categoria);
        $sentencia->execute();
        $sentencia->bind_result($id, $descripcion, $precio, $categoria);

        // Obtener los productos:
        $respuesta = [];
        while ($sentencia->fetch()) {
            $producto = new \stdClass();
            $producto->id          = $id;
            $producto->descripcion = $descripcion;
            $producto->precio      = $precio;
            $producto->categoria   = $categoria;

            $respuesta[] = $producto;
        }
        $sentencia->free_result();
        
    } else {
        // No se pudo realizar la consulta.
        $respuesta->error = $conexion->error;
    }
}
$conexion->close();
echo json_encode($respuesta);
?>